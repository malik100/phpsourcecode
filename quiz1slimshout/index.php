
<?php


error_reporting(E_ALL);
ini_set('display_errors', 'On');
session_start();
require_once 'vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;

    DB::$dbName = 'quiz1slimshout';
    DB::$user = 'quiz1slimshout';
    DB::$password = '1234';
    DB::$host = 'localhost';
    DB::$port = 3333;


// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1','VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/errors.log', Logger::ERROR));

//adding global session
$container['view']->getEnvironment()->addGlobal('userSession', $_SESSION['user'] ?? null );


//show form for register page
$app->get('/register', function ($request, $response, $args) use($log) {
    return $this->view->render($response, "register.html.twig", []);
});

//post form for register page
$app->post('/register', function ($request, $response, $args) use($log) {
    $name = $request->getParam('username');
    $password = $request->getParam('password');
    $errorList = [];
    if(strlen($name) < 5 || strlen($name) > 20){
        $name = "";
        $errorList[] = "Name must be between 5-20 characters";
    }
    if(strlen($password) < 5 || strlen($password) > 100){
        $password = "";
        $errorList[] = "Password must be between 5-100 characters";
    }
    if($errorList){
        return $this->view->render($response, "register.html.twig",
     ['name' => $name, 'password' => $password, 'errorList' => $errorList]);
    }else{
        DB::insert('users', ['username' => $name, 'password' => $password, 'imagePath' => $name . "sphoto"]);
        return $this->view->render($response, "register_success.html.twig", []);
    }
    
});

//show home page
$app->get('/', function ($request, $response, $args) use($log) {
    return $this->view->render($response, "homepage.html.twig", []);
});

//show internal error page
$app->get('/internalerror', function ($request, $response, $args) use($log) {
    return $this->view->render($response, "internalerror.html.twig", []);
});

//show login page
$app->get('/login', function ($request, $response, $args) use($log) {
    return $this->view->render($response, "login.html.twig", []);
});

// loginpage post version
$app->post('/login', function ($request, $response, $args) use($log) {
    $username = $request->getParam('username');
    $password = $request->getParam('password');
    $errorList = [];
    if($username == ""){
        $errorList[] = "incorrect";
    }
    if($password == ""){
        $errorList[] = "incorrect";
    }
    $match = DB::queryFirstRow("SELECT * from users where username='$username'");
    if(isset($match)){
        if($password === $match['password']){
            unset($mtch['password']);
            $_SESSION['user'] = $match;
            $log->debug($_SESSION['user']['username'] . ' id #'. $_SESSION['user']['id'] . ' logged in successfully');
            return $response->withRedirect("/");
        }
    }
    $errorList[] = "incorrect";
    $password = "";
    $log->info("a user login attempt failed");
    return $this->view->render($response, "login.html.twig", ['errorList' => $errorList]);
    
    
});

//logout page
$app->get('/logout', function ($request, $response, $args) use($log) {
    if(isset($_SESSION['user'])){
        unset($_SESSION['user']);
    }
    return $this->view->render($response, "logout.html.twig", []);
});

//shouts list page
$app->get('/shouts/list', function ($request, $response, $args) use($log) {
    $shouts = DB::query("SELECT * FROM shouts as s, users as u where s.authorid = u.id");
    $usersOnly = DB::query("SELECT * FROM users");
    return $this->view->render($response, "shoutslist.html.twig", ['shouts' => $shouts, 'usersOnly' => $usersOnly]);
});



//show add shouts page
$app->get('/shouts/add', function ($request, $response, $args) use($log) {
    if(!isset($_SESSION['user'])){
        //directing toward forbidden page
        $log->warning("access denied");
        return $this->view->render($response, "forbidden.html.twig", []);
    }
    return $this->view->render($response, "addshouts.html.twig", []);
});

$app->post('/shouts/add', function ($request, $response, $args) use($log) {
    $currUser = $_SESSION['user'];
    $authorId = $currUser['id'];
    $message = $request->getParam('message');
    $errorList = [];
    if(strlen($message) < 1 || strlen($message) > 100){
        $errorList[] = "Message length must be between 1-100 characters";
    }
    if($errorList){
        return $this->view->render($response, "addshouts.html.twig", ['message' => $message, 'errorList' => $errorList]);
    }else{
        DB::insert('shouts', ['authorId' => $authorId, 'message' => $message]);
        $log->debug($_SESSION['user']['username'] . ' id #'. $_SESSION['user']['id'] . ' added a shout successfully');
        return $this->view->render($response, "addshouts_success.html.twig", ['message' => $message, 'errorList' => $errorList]);
    }
});


// Run app
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    global $log;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    // redirect
    header("Location: /internalerror");
    die;
}

//ajax
$app->get('/ajax/loadshouts/[{id}]', function ($request, $response, $args) {
    $id = isset($args['id']) ? $args['id'] : "";
    $record = DB::queryFirstRow("SELECT * FROM users WHERE id=%s", $id);
    if ($record) {
        //debugging by showing username
        return $response->write($record['username']);
    } else {
        return $response->write("");
    }
});

$app->run();
















