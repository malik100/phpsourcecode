<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* homepage.html.twig */
class __TwigTemplate_b917c941b030dbe958ee878ac851de11af06e5ec32c47683ff59173892ded346 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "homepage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    
    <h1>Home Page</h1>
    ";
        // line 5
        if (($context["userSession"] ?? null)) {
            // line 6
            echo "        <h2><a href=\"/shouts/list\">See all shouts</a></h2>
        <h2><a href=\"/shouts/add\">Add a shout</a></h2>
    ";
        } else {
            // line 8
            echo "    
    <h2><a href=\"/shouts/list\">See all shouts</a></h2>
        <h2><a href=\"/login\">Login first to add shouts</a></h2>
    ";
        }
        // line 12
        echo "
    

";
    }

    public function getTemplateName()
    {
        return "homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 12,  61 => 8,  56 => 6,  54 => 5,  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block body %}
    
    <h1>Home Page</h1>
    {% if userSession %}
        <h2><a href=\"/shouts/list\">See all shouts</a></h2>
        <h2><a href=\"/shouts/add\">Add a shout</a></h2>
    {% else %}    
    <h2><a href=\"/shouts/list\">See all shouts</a></h2>
        <h2><a href=\"/login\">Login first to add shouts</a></h2>
    {% endif  %}

    

{% endblock %}", "homepage.html.twig", "C:\\xampp\\htdocs\\quiz1slimshout\\templates\\homepage.html.twig");
    }
}
