<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* addshouts.html.twig */
class __TwigTemplate_bb707e47d30e540cbfcca31cba1165ddef8ae7923c1cf012dc28fdeab2ff14c4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "addshouts.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
        if (($context["userSession"] ?? null)) {
            // line 4
            echo "    <h1>Add a shout</h1>
    <h2>enter shout message below:</h2>
    <form method=\"post\">
        <textarea name=\"message\" id=\"mes\" cols=\"100\" rows=\"10\">";
            // line 7
            echo twig_escape_filter($this->env, ($context["message"] ?? null), "html", null, true);
            echo "</textarea><br><br>
        <input type=\"submit\" value=\"post shout\">
    </form>
    ";
        } else {
            // line 11
            echo "        <h2>Only registered users can post shouts please use above links to sign in or register</h2>
    ";
        }
        // line 13
        echo "    ";
        if (($context["errorList"] ?? null)) {
            // line 14
            echo "        <ul class=\"lists\">
            ";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 16
                echo "                <li> ";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo " </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "        </ul>
    ";
        }
    }

    public function getTemplateName()
    {
        return "addshouts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 18,  79 => 16,  75 => 15,  72 => 14,  69 => 13,  65 => 11,  58 => 7,  53 => 4,  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block body %}
    {% if userSession %}
    <h1>Add a shout</h1>
    <h2>enter shout message below:</h2>
    <form method=\"post\">
        <textarea name=\"message\" id=\"mes\" cols=\"100\" rows=\"10\">{{message}}</textarea><br><br>
        <input type=\"submit\" value=\"post shout\">
    </form>
    {% else %}
        <h2>Only registered users can post shouts please use above links to sign in or register</h2>
    {% endif %}
    {% if errorList %}
        <ul class=\"lists\">
            {% for error in errorList %}
                <li> {{error}} </li>
            {% endfor %}
        </ul>
    {% endif %}
{% endblock %}

", "addshouts.html.twig", "C:\\xampp\\htdocs\\quiz1slimshout\\templates\\addshouts.html.twig");
    }
}
