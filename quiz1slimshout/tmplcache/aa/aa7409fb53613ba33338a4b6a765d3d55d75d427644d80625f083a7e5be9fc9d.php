<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_d3b2532a9553140c4d94041989b7dde2e0560d1e767f59acb15ccd5028a5891f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'tile' => [$this, 'block_tile'],
            'head' => [$this, 'block_head'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <style>
        body{
            background-color: lightblue;
            margin: 0 auto;
            padding: 0 auto;
            text-align:center;
            width: 960px;
        }
        .shoutBox{
            background-color: lightcyan;
            padding: 3em;
            margin: 2em;
            font-size: 1.1em;
            text-align: justify;
        }
        .lists{
            list-style: none;
        }
    </style>
    <title>";
        // line 26
        $this->displayBlock('tile', $context, $blocks);
        echo "</title>
    ";
        // line 27
        $this->displayBlock('head', $context, $blocks);
        // line 28
        echo "</head>
<body>
    ";
        // line 30
        $this->displayBlock('header', $context, $blocks);
        // line 37
        echo "    ";
        $this->displayBlock('body', $context, $blocks);
        // line 38
        echo "</body>
</html>


";
    }

    // line 26
    public function block_tile($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 27
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 30
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "        ";
        if (($context["userSession"] ?? null)) {
            // line 32
            echo "            <h3>You are logged in as ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userSession"] ?? null), "username", [], "any", false, false, false, 32), "html", null, true);
            echo "   <a href=\"/logout\">logout?</a></h3>
        ";
        } else {
            // line 34
            echo "        <h3><a href=\"/login\">Please sign in</a> or <a href=\"/register\">Register</a> to take part in shouts</h3>
        ";
        }
        // line 36
        echo "    ";
    }

    // line 37
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  124 => 37,  120 => 36,  116 => 34,  110 => 32,  107 => 31,  103 => 30,  97 => 27,  91 => 26,  83 => 38,  80 => 37,  78 => 30,  74 => 28,  72 => 27,  68 => 26,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <style>
        body{
            background-color: lightblue;
            margin: 0 auto;
            padding: 0 auto;
            text-align:center;
            width: 960px;
        }
        .shoutBox{
            background-color: lightcyan;
            padding: 3em;
            margin: 2em;
            font-size: 1.1em;
            text-align: justify;
        }
        .lists{
            list-style: none;
        }
    </style>
    <title>{% block tile %}{% endblock %}</title>
    {% block head %}{% endblock %}
</head>
<body>
    {% block header %}
        {% if userSession %}
            <h3>You are logged in as {{userSession.username}}   <a href=\"/logout\">logout?</a></h3>
        {% else %}
        <h3><a href=\"/login\">Please sign in</a> or <a href=\"/register\">Register</a> to take part in shouts</h3>
        {% endif %}
    {% endblock %}
    {% block body %}{% endblock %}
</body>
</html>


", "master.html.twig", "C:\\xampp\\htdocs\\quiz1slimshout\\templates\\master.html.twig");
    }
}
