<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_959f9e7b1b1e0c70130cbcea26a3fa119ab2b228950472b43aabc533688fc666 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    <br><br>
    <form method=\"post\">
        <label for=\"username\">Username:</label>
        <input type=\"text\" name=\"username\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "\"><br><br>
        <label for=\"password\">Password:</label>
        <input type=\"text\" name=\"password\"><br><br>
        <input type=\"submit\" value=\"Register\">
    </form>  
    ";
        // line 11
        if (($context["errorList"] ?? null)) {
            // line 12
            echo "        <ul class=\"lists\">
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 14
                echo "                <li> ";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo " </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "        </ul>
    ";
        }
        // line 18
        echo "
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 18,  81 => 16,  72 => 14,  68 => 13,  65 => 12,  63 => 11,  55 => 6,  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block body %}
    <br><br>
    <form method=\"post\">
        <label for=\"username\">Username:</label>
        <input type=\"text\" name=\"username\" value=\"{{name}}\"><br><br>
        <label for=\"password\">Password:</label>
        <input type=\"text\" name=\"password\"><br><br>
        <input type=\"submit\" value=\"Register\">
    </form>  
    {% if errorList %}
        <ul class=\"lists\">
            {% for error in errorList %}
                <li> {{error}} </li>
            {% endfor %}
        </ul>
    {% endif %}

{% endblock %}", "register.html.twig", "C:\\xampp\\htdocs\\quiz1slimshout\\templates\\register.html.twig");
    }
}
