<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_d11340fe9e958e9f68503b3c4751901d48a122d652f66cdbc672607c264c0e16 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    
    <h1>Login</h1>
    ";
        // line 5
        if (twig_get_attribute($this->env, $this->source, ($context["userSession"] ?? null), "user", [], "any", false, false, false, 5)) {
            // line 6
            echo "        <h2><a href=\"/\">already logged in go to main page</a></h2>
    ";
        } else {
            // line 7
            echo "    
    <form method=\"post\">
        <label for=\"username\">Username:</label>
        <input type=\"text\" name=\"username\" ><br><br>
        <label for=\"password\">Password:</label>
        <input type=\"text\" name=\"password\" ><br><br>
        <input type=\"submit\" value=\"login\">
    </form>  
    <h1>";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["m"] ?? null), "name", [], "any", false, false, false, 15), "html", null, true);
            echo "</h1>
    <h1>";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["m"] ?? null), "password", [], "any", false, false, false, 16), "html", null, true);
            echo "</h1>
    ";
        }
        // line 18
        echo "    ";
        if (($context["errorList"] ?? null)) {
            // line 19
            echo "        <h1>Incorrct email or password please try again</h1>
    ";
        }
        // line 21
        echo "
";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 21,  82 => 19,  79 => 18,  74 => 16,  70 => 15,  60 => 7,  56 => 6,  54 => 5,  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block body %}
    
    <h1>Login</h1>
    {% if userSession.user %}
        <h2><a href=\"/\">already logged in go to main page</a></h2>
    {% else %}    
    <form method=\"post\">
        <label for=\"username\">Username:</label>
        <input type=\"text\" name=\"username\" ><br><br>
        <label for=\"password\">Password:</label>
        <input type=\"text\" name=\"password\" ><br><br>
        <input type=\"submit\" value=\"login\">
    </form>  
    <h1>{{m.name}}</h1>
    <h1>{{m.password}}</h1>
    {% endif  %}
    {% if errorList %}
        <h1>Incorrct email or password please try again</h1>
    {% endif %}

{% endblock %}", "login.html.twig", "C:\\xampp\\htdocs\\quiz1slimshout\\templates\\login.html.twig");
    }
}
