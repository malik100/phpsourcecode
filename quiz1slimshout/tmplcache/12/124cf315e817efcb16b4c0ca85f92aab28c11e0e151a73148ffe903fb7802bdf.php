<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* shoutslist.html.twig */
class __TwigTemplate_da6d0c60102256fcaea7994b69064dcb1ff73753ce0c0aead87156af3d82fb76 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "shoutslist.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function() {
            \$('select').on('change', function() {
                var user = \$(this.value);
                \$(\"#userpost\").load(\"/ajax/loadshouts/\" + user);
            });

            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

        });
    </script>
    ";
        // line 18
        if (($context["userSession"] ?? null)) {
            // line 19
            echo "        <h2><a href=\"/shouts/add\">add your own shout!</a></h2>
    ";
        }
        // line 21
        echo "    <h1>All shouts</h1>
    
    <form method=\"get\">
        <select name=\"users\" id=\"userPicked\">
            <option value=\"\">Please select a user</option>
            ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["usersOnly"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 27
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "id", [], "any", false, false, false, 27), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "username", [], "any", false, false, false, 27), "html", null, true);
            echo "</option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        </select>
        <input type=\"submit\" value=\"select\">
    </form>
    <span id=\"userpost\"></span>
    ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["shouts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 34
            echo "    <div class=\"shoutBox\">
        <h2>Shout #";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "id", [], "any", false, false, false, 35), "html", null, true);
            echo " posted by ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "username", [], "any", false, false, false, 35), "html", null, true);
            echo "</h2>
        <p>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "message", [], "any", false, false, false, 36), "html", null, true);
            echo "</p>
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "
";
    }

    public function getTemplateName()
    {
        return "shoutslist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 39,  114 => 36,  108 => 35,  105 => 34,  101 => 33,  95 => 29,  84 => 27,  80 => 26,  73 => 21,  69 => 19,  67 => 18,  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block body %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function() {
            \$('select').on('change', function() {
                var user = \$(this.value);
                \$(\"#userpost\").load(\"/ajax/loadshouts/\" + user);
            });

            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });

        });
    </script>
    {% if userSession %}
        <h2><a href=\"/shouts/add\">add your own shout!</a></h2>
    {% endif %}
    <h1>All shouts</h1>
    
    <form method=\"get\">
        <select name=\"users\" id=\"userPicked\">
            <option value=\"\">Please select a user</option>
            {% for u in usersOnly %}
            <option value=\"{{u.id}}\">{{u.username}}</option>
            {% endfor %}
        </select>
        <input type=\"submit\" value=\"select\">
    </form>
    <span id=\"userpost\"></span>
    {% for s in shouts %}
    <div class=\"shoutBox\">
        <h2>Shout #{{s.id}} posted by {{s.username}}</h2>
        <p>{{s.message}}</p>
    </div>
    {% endfor %}

{% endblock %}

", "shoutslist.html.twig", "C:\\xampp\\htdocs\\quiz1slimshout\\templates\\shoutslist.html.twig");
    }
}
