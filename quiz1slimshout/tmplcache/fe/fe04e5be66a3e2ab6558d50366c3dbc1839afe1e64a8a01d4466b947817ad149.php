<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* addshouts_success.html.twig */
class __TwigTemplate_ba409195dad54b426aa629eefde6a33099de8829509714abb8d426f4726a1da1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "addshouts_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    
    <h1>Successfully added shout</h1>
    <h2><a href=\"/shouts/list\">see shouts now</a> or <a href=\"/shouts/add\">add another</a></h2>

";
    }

    public function getTemplateName()
    {
        return "addshouts_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block body %}
    
    <h1>Successfully added shout</h1>
    <h2><a href=\"/shouts/list\">see shouts now</a> or <a href=\"/shouts/add\">add another</a></h2>

{% endblock %}", "addshouts_success.html.twig", "C:\\xampp\\htdocs\\quiz1slimshout\\templates\\addshouts_success.html.twig");
    }
}
