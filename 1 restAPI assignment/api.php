<?php

require_once 'vendor/autoload.php';

use Respect\Validation\Validator as Validator;

use Slim\Http\Request;
use Slim\Http\Response;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Respect\Validation\Rules\Json;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/errors.log', Logger::ERROR));

DB::$dbName = 'restauctions';
DB::$user = 'restauctions';
DB::$password = '7[oW7gRXYz-WkGZu';
DB::$port = 3333;

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params)
{
    global $log;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    http_response_code(500); // internal server error
    header('Content-type: application/json; charset=UTF-8');
    die(json_encode("500 - Internal server error"));
}


use Slim\Http\UploadedFile;

// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// File upload directory
$container['upload_directory'] = __DIR__ . '/uploads';

//Override the default Not Found Handler before creating App
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    };
};

$passwordPepper = 'mmyb7oSAeXG9DTz2uFqu';

// set content-type globally using middleware (untested)
$app->add(function ($request, $response, $next) {
    //sleep(1); // artificially delay all responses by 1 second
    $response = $next($request, $response);
    return $response->withHeader('Content-Type', 'application/json; charset=UTF-8');
});


// ============= URL HANDLERS BELOW THIS LINE ========================

$app->get('/', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Auctions app RESTful API");
    return $response;
});

$app->group('/api', function () use ($app) {

    //--------------GET HANDLERS-----------------------------------
    $app->get('/auctions', function (Request $request, Response $response, array $args) {
        $list = DB::query("SELECT * FROM auctions");
        $json = json_encode($list, JSON_PRETTY_PRINT);
        return $response->getBody()->write($json);
    });

    $app->get('/auctions/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
        $item = DB::queryFirstRow("SELECT * FROM auctions WHERE id=%i", $args['id']);
        //--------IF ITEM NOT FOUND OR NO ACCESS----------------------------
        if (!$item) {
            $response = $response->withStatus(404);
            $response->getBody()->write(json_encode("404 - not found"));
            return $response;
        }
        $json = json_encode($item, JSON_PRETTY_PRINT);
        return $response->getBody()->write($json);
    });
    //---------------------POST HANDLER------------------------------------------------
    $app->post('/auctions', function (Request $request, Response $response, array $args) {
        $json = $request->getBody();
        $item = json_decode($json, TRUE);
        // validation of auction
        if (($result = validateAuction($item)) !== TRUE) {
            $response = $response->withStatus(400);
            $response->getBody()->write(json_encode("400 - " . $result));
            return $response;
        }
        DB::insert('auctions', $item);
        $insertId = DB::insertId();
        $response = $response->withStatus(201); // created
        $response->getBody()->write(json_encode($insertId));
        return $response;
    });
    //-----------------PATCH HANDLER--------------------------------------------------
    $app->patch('/auctions/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
        $json = $request->getBody();
        $item = json_decode($json, TRUE);
        $origItem = DB::queryFirstRow("SELECT * FROM auctions WHERE id=%i", $args['id']);
        // validate
        if (($result = validateForPatch($item, $origItem) )!== TRUE) {
            $response = $response->withStatus(400);
            $response->getBody()->write(json_encode("400 - " . $result));
            return $response;
        }
        DB::update('auctions', $item, "id=%i", $args['id']);
        $response->getBody()->write(json_encode(true));
        return $response;
    });

});
// returns TRUE if todo is valid, otherwise returns a string describing the problem

function validateAuction($auction){
    if($auction == NULL){
        return "Invalid JSON data provided";
    }
    $expectedFields = ['itemDesc', 'sellerEmail'];
    $auctionFields = array_keys($auction);
    if($diff = array_diff($auctionFields, $expectedFields)){
        return "Invalid fields in auction: [" . implode(',', $diff) . "]";
    }
    if($diff = array_diff($expectedFields ,$auctionFields)){
        return "Missing fields in auction: [" . implode(',', $diff) . "]";
    }
    if(filter_var($auction['sellerEmail'], FILTER_VALIDATE_EMAIL) === false){
        return "Email is invalid";
    }
    if(isset($auction['itemDesc'])){
        if(strlen($auction['itemDesc']) < 1 || strlen($auction['itemDesc']) > 200){
            return "Item description must be between 1-100 characters";
        }
    }
    if(isset($auction['sellerEmail'])){
        if(strlen($auction['sellerEmail']) < 1 || strlen($auction['sellerEmail']) > 200){
            return "Email must be a maximum of 320 characters";
        }
    }
    return TRUE;
}

function validateForPatch($auction, $original){
    if($auction == NULL){
        return "Invalid JSON data provided";
    }
    $expectedFields = ['lastBid', 'lastBidderEmail'];
    $auctionFields = array_keys($auction);
    if($diff = array_diff($auctionFields, $expectedFields)){
        return "Invalid fields in auction: [" . implode(',', $diff) . "]";
    }
    if($diff = array_diff($expectedFields ,$auctionFields)){
        return "Missing fields in auction: [" . implode(',', $diff) . "]";
    }
    if($auction['lastBid'] <= $original['lastBid']){
        return "New bid must be higher than old bid";
    }
    if(filter_var($auction['lastBidderEmail'], FILTER_VALIDATE_EMAIL) === false){
        return "Email is invalid";
    }
    if(isset($auction['lastBidderEmail'])){
        if(strlen($auction['lastBidderEmail']) > 320){
            return "Email must be a maximum of 320 characters";
        }
    }
    return TRUE;
}
// Run app
$app->run();
