-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jun 22, 2021 at 06:22 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restauctions`
--

-- --------------------------------------------------------

--
-- Table structure for table `auctions`
--

CREATE TABLE `auctions` (
  `id` int(11) NOT NULL,
  `itemDesc` varchar(200) NOT NULL,
  `sellerEmail` varchar(320) NOT NULL,
  `lastBid` decimal(10,2) NOT NULL,
  `lastBidderEmail` varchar(320) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auctions`
--

INSERT INTO `auctions` (`id`, `itemDesc`, `sellerEmail`, `lastBid`, `lastBidderEmail`) VALUES
(1, 'A good Toaster', 'john@email.com', '15.00', 'Lee@email.com'),
(2, 'Garden Gnome', 'mark@email.com', '105.00', 'Lucy@email.com'),
(4, 'A used laptop', 'Kenny@email.com', '0.00', ''),
(7, 'a used lawnmower', 'ken@email.com', '1.00', 'moe@email.ca'),
(8, 'a keyboard', 'victor@email.com', '35.50', 'joe2@email.com'),
(9, 'a treadmill', 'mark@email.com', '0.00', ''),
(10, 'a skateboard', 'mark@email.ca', '0.00', ''),
(11, 'a bicycle', 'maggie@email.com', '0.00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auctions`
--
ALTER TABLE `auctions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auctions`
--
ALTER TABLE `auctions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
