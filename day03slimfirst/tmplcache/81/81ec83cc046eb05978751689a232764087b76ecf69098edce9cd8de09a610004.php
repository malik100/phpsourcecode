<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* addperson.html.twig */
class __TwigTemplate_5ef617e2da5cbe0e39b41f45099f6aba35e0c709cea765262a03b404f3f9471f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "addperson.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Add Person";
    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        if (($context["errorList"] ?? null)) {
            // line 6
            echo "<ul>
";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 8
                echo "    <li class=\"errorMessage\">";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "</ul>
";
        }
        // line 12
        echo "
<form method=\"POST\">
    Name: <input type=\"text\" name =\"name\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 14), "html", null, true);
        echo "\"><br>
    Age: <input type=\"number\" name=\"age\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "age", [], "any", false, false, false, 15), "html", null, true);
        echo "\"><br>
    <input type=\"submit\" value=\"Add person\">
</form>

";
    }

    public function getTemplateName()
    {
        return "addperson.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 15,  84 => 14,  80 => 12,  76 => 10,  67 => 8,  63 => 7,  60 => 6,  58 => 5,  54 => 4,  47 => 3,  36 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("
{% extends \"master.html.twig\" %}
{% block title %}Add Person{% endblock title %}
{% block content %}
{% if errorList %}
<ul>
{% for error in errorList %}
    <li class=\"errorMessage\">{{error}}</li>
{% endfor %}
</ul>
{% endif %}

<form method=\"POST\">
    Name: <input type=\"text\" name =\"name\" value=\"{{v.name}}\"><br>
    Age: <input type=\"number\" name=\"age\" value=\"{{v.age}}\"><br>
    <input type=\"submit\" value=\"Add person\">
</form>

{% endblock content %}", "addperson.html.twig", "C:\\xampp\\htdocs\\ipd24\\day03slimfirst\\templates\\addperson.html.twig");
    }
}
