<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_d3b2532a9553140c4d94041989b7dde2e0560d1e767f59acb15ccd5028a5891f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    <link rel=\"stylesheet\" href=\"styles.css\">
</head>
<body>
    <div id=\"centeredContent\">
        ";
        // line 12
        $this->displayBlock('content', $context, $blocks);
        // line 13
        echo "        <div id=\"footer\">&copy; Copyright 2011 by <a href=\"http://domain.invalid/\">you</a>.</div>
    </div>
</body>
</html>";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "default";
    }

    // line 12
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  71 => 12,  64 => 7,  57 => 13,  55 => 12,  47 => 7,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <title>{% block title %}default{% endblock %}</title>
    <link rel=\"stylesheet\" href=\"styles.css\">
</head>
<body>
    <div id=\"centeredContent\">
        {% block content %}{% endblock %}
        <div id=\"footer\">&copy; Copyright 2011 by <a href=\"http://domain.invalid/\">you</a>.</div>
    </div>
</body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\ipd24\\day03slimfirst\\templates\\master.html.twig");
    }
}
