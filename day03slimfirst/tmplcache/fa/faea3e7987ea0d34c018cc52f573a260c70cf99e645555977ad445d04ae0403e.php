<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hello.html.twig */
class __TwigTemplate_3f2eb87356f96cb84b8e28718c5be2c28132580d176e15843e3d4d805dc2376b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
<p>Hello (from twig) <i>";
        // line 2
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "</i> you are <b>";
        echo twig_escape_filter($this->env, ($context["age"] ?? null), "html", null, true);
        echo "</b> y/o</p>

<p>Record with id ";
        // line 4
        echo twig_escape_filter($this->env, ($context["insertid"] ?? null), "html", null, true);
        echo " created</p>";
    }

    public function getTemplateName()
    {
        return "hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 4,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
<p>Hello (from twig) <i>{{name}}</i> you are <b>{{age}}</b> y/o</p>

<p>Record with id {{insertid}} created</p>", "hello.html.twig", "C:\\xampp\\htdocs\\ipd24\\day03slimfirst\\templates\\hello.html.twig");
    }
}
