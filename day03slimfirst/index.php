
<?php

// for development we want to see all the errors, some php.ini versions disable those (e.g. MAMP)
error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once 'vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;
/*
DB::$dbName = 'day03people';
DB::$user = 'day03people';
DB::$password = '8kbsec9.q.PDd4xm';
DB::$host = 'localhost';
DB::$port = 3333;
*/
    DB::$dbName = 'day03people';
    DB::$user = 'day03people';
    DB::$password = 'K4i!4cnRs8!Sw5/@';
    DB::$host = 'localhost';
    DB::$port = 3333;
    /*
if (strpos($_SERVER['HTTP_HOST'], "ipd24.ca") !== false) {
    // hosting on ipd24.ca
    DB::$dbName = 'cp5003_slimfirst';
    DB::$user = 'cp5003_slimfirst';
    DB::$password = 'DD6SZ%IXA3.0';
} else { // local computer
    
}
*/


/*
DB::$dbName = 'day03people';
DB::$user = 'day03people';
DB::$password = '8kbsec9.q.PDd4xm';
DB::$host = 'localhost';
DB::$port = 3333;

name = cp5003_slimfirst
pass = DD6SZ%IXA3.0
*/

// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1','VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/errors.log', Logger::ERROR));


// Define app routes
$app->get('/hello/{name}', function ($request, $response, $args) {
    return $response->write("day03slim -> Hello " . $args['name']);
});

$app->get('/hello/{namePPP}/{agePPP:[0-9]+}', function ($request, $response, $args) {
    $name = $args['namePPP'];
    $age = $args['agePPP'];
    DB::insert('people', ['name' => $name, 'age' => $age]);
    $insertId = DB::insertId();
    return $this->view->render($response, 'hello.html.twig', ['ageTTT' => $age, 'nameTTT' => $name, 'insertIdTTT' => $insertId]);
    // return $response->write("<p>Hello $name, you are <b>$age</b> y/o</p>");
});

// STATE 1: first display of the form
$app->get('/addperson', function ($request, $response, $args) {    
    return $this->view->render($response, 'addperson.html.twig');
});

// STATE 2&3: receiving form submission
$app->post('/addperson', function (Request $request, Response $response, $args) {    
    $name = $request->getParam('name');
    $age = $request->getParam('age');
    // validation
    $errorList = [];
    if (strlen($name) < 2 || strlen($name) > 100) {
        $name = "";
        $errorList[] = "Name must be 2-100 characters long"; // append to array
    }
    if (filter_var($age, FILTER_VALIDATE_INT) === false || $age < 0 || $age > 150) {
        $age = "";
        $errorList[] = "Age must be a number between 0 and 150";
    }
    //
    if ($errorList) { // STATE 2: errors - show and redisplay the form
        $valuesList = ['name' => $name, 'age' => $age];
        return $this->view->render($response, "addperson.html.twig", ['errorList' => $errorList, 'v' => $valuesList]);
    } else { // STATE 3: success
        DB::insert('peopleaaa', ['name' => $name, 'age' => $age]);
        return $this->view->render($response, "addperson_success.html.twig");
    }
});
//$log->debug("User Logged In >>>>>>>>>>>");
//$log->error("User Logged In <<<<<<<<<<<<>>>>>>>>>>>");
//$log->alert("TEST");
//$log->debug(sprintf("Logout successful for uid=%d, from %s", @$_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
// Run app
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    global $log;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    // redirect
    header("Location: /internalerror");
    die;
}

$app->run();

